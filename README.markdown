# Around The Block

## Decentralized restaurant reviews

### The Problem

The majority of review sites aggregate, curate, and data mine reviews donated by users. This leads to a single entity with the power to alter, censor, and profit from data generously provided by its users. These users then become the product the company sells to advertisers and spammers.

### The Solution

*Around The Block* is the first restaurant review site to forego the ability to censor or alter user reviews. By using blockchain technology via [Blockstack](https://blockstack.org/), *Around The Block* uses a decentralized identity and storage system to save your reviews in a space not controlled by us.

This means that anyone can collect, review, or analyze the reviews without any sort of approval or censorship. The subsequent reviews in *Around The Block* are authentic and not only uncensored, but technically uncensorable.

*Around The Block* merely offers a user interface for review creation and stores a cache of the reviews, which anyone else could freely do, too.

### How It Works

"Around The Block" is a distributed application running over [Blockstack](https://github.com/blockstack/blockstack) which offers a simple user interface to store all user-generated reviews in their own decentralized storage on [gaia](https://github.com/blockstack/gaia).

### Who We Are

We are [JUMPstartups](https://jumpstartups.io), a consulting company tailoring to startups in the NYC area.