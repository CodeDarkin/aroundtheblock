"""aroundtheblock URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from aroundtheblockapp import views
from django.conf.urls import url


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', views.SearchView.as_view(), name='searchview'),
    url(r'^manifest.json$', views.ManifestView.as_view(), name='manifestview'),
    url(r'^restaurant/(?P<id>\d+)$', views.RestaurantView.as_view(), name='restaurant'),
    url(r'^restaurant/create$', views.CreateRestaurantView.as_view(), name='create_restaurant'),
    url(r'^restaurant/(?P<restaurant_id>\d+)/review$', views.ReviewView.as_view(), name='createreview'),
    url(r'^restaurant/(?P<restaurant_id>\d+)/review/(?P<review_id>\d+)$', views.ReviewView.as_view(), name='editreview'),
    url(r'^api/search$', views.JSONSearchView.as_view(), name='api_search'),
]
