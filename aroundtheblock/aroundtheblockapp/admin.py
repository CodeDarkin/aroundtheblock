from django.contrib import admin
from .models import BlockstackUser, Restaurant, Review

# Register your models here.


admin.site.register([BlockstackUser, Restaurant, Review])
