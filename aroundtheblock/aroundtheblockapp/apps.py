from django.apps import AppConfig


class AroundtheblockappConfig(AppConfig):
    name = 'aroundtheblockapp'
