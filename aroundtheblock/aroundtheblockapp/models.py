from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User

# Create your models here.

class BlockstackUser(models.Model):
    email = models.TextField(null=True)
    username = models.TextField(unique=True)
    joined = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.username

class Restaurant(models.Model):
    name = models.TextField()
    address = models.TextField()
    added = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(BlockstackUser, on_delete=models.PROTECT)

    def __str__(self):
        return self.name

class Review(models.Model):
    user = models.ForeignKey(BlockstackUser, on_delete=models.PROTECT)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.PROTECT)
    stars= models.IntegerField()
    description = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Review #{} of {} by {}'.format(self.id, self.restaurant.name, self.user.username)

