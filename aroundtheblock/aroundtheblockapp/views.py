from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseForbidden
from django.views.generic import TemplateView
from .models import Restaurant, Review, BlockstackUser
from django.views import View
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.db.models import Count, Avg

def qs_restaurantsWithReviewCounts():
    return Restaurant.objects.annotate(review_count=Count('review'))
    
class SearchView(TemplateView):
    template_name="aroundtheblockapp/index.html"
    def post(self, request):
        keywords = request.POST["keywords"]
        matches = qs_restaurantsWithReviewCounts().\
                  filter(name__icontains=keywords) \
                  .order_by('-review_count')
        return render(request, self.template_name, {'keywords': keywords,  'matches':matches})

    def get(self, request):
        featured = Restaurant.objects.annotate(review_count=Count('review')).order_by('-review_count')[:3]
        return render(request, self.template_name, {'featured': featured})

#special search view for API use (chrome plugin)
@method_decorator(csrf_exempt, name='dispatch')
class JSONSearchView(View):
    def post(self, request):
        name = request.POST["name"]
        address = request.POST["address"]
        matches = Restaurant.objects.filter(name__icontains=name).annotate(review_count=Count('review')).order_by('-review_count').all() #, address__iexact=address).all()
        if len(matches) == 0:
            return JsonResponse({})
        #just return the first match
        restaurant = matches[0]
        res = model_to_dict(restaurant)
        res['added_by'] = restaurant.added_by.username
        jsonres = {'restaurant': res, 'reviews':[]}
        for review in Review.objects.filter(restaurant=restaurant).order_by('-timestamp'):
           jsonres['reviews'].append(model_to_dict(review))
           jsonres['reviews'][-1]['username'] = review.user.username
        return JsonResponse(jsonres)

class ManifestView(View):
    def get(self, request):
        return HttpResponse("""{
  "name": "Around the Block",
  "start_url": "127.0.0.1:8000",
  "description": "User-controlled Dining Reviews",
  "icons": [{
    "src": "https://aroundtheblock.jumpstartups.io/static/images/aroundtheblock.png",
    "sizes": "192x192",
    "type": "image/png"
  }]
}""", content_type="application/json")

def blockstackUserForName(username):
   blockstackuser = BlockstackUser.objects.filter(username=username).all()
   if len(blockstackuser) == 1:
       return blockstackuser[0]

@method_decorator(csrf_exempt, 'dispatch')   
class RestaurantView(TemplateView):
    template_name = "aroundtheblockapp/restaurant.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        username = self.request.COOKIES.get('username')
        blockstackuser = blockstackUserForName(username)
        
        if blockstackuser:
            context['my_reviews'] = Review.objects.filter(restaurant_id=kwargs['id']).filter(user=blockstackuser)
          
        o_reviews = Review.objects.filter(restaurant_id=kwargs['id'])
        if blockstackuser:
            o_reviews = o_reviews.exclude(user=blockstackuser)

        context['user_review_count'] = {}
        for user in BlockstackUser.objects.annotate(num_reviews=Count('review')):
            context['user_review_count'][user.username] = user.num_reviews

        for review in o_reviews:
            review.user_review_count = Review.objects.filter(user=review.user).count()
        context['other_reviews'] = o_reviews
        context['restaurant'] = Restaurant.objects.get(pk=kwargs['id'])
        context['restaurant'].average_rating = Review.objects.filter(restaurant=context['restaurant']).all().aggregate(avg_stars=Avg('stars'))['avg_stars']
        return context
    def render_to_response(self, context):
       if self.request.is_ajax():
           restaurant = model_to_dict_extra(context['restaurant'], extra_fields=['average_rating'])
           restaurant['added_by'] = context['restaurant'].added_by.username
           jsonctx = {'restaurant': restaurant, 'other_reviews':[]}
           for review in context['other_reviews']:
               jsonctx['other_reviews'].append(model_to_dict_extra(review,extra_fields=['user_review_count']))
               jsonctx['other_reviews'][-1]['username'] = review.user.username
           return JsonResponse(jsonctx)
       else:
          return super().render_to_response(context)

class CreateRestaurantView(View):
    def post(self, request, *args, **kwargs):
        name = request.POST['name']
        address = request.POST['address']
        username = request.COOKIES.get('username')
        email = request.COOKIES.get('email')
        if username is None: #no blockstack user is logged in
            return HttpResponseForbidden()
        
        blockstackuser = blockstackUserForName(username)
        
        if not blockstackuser:
            blockstackuser = BlockstackUser(email=email, username=username)
            blockstackuser.save()

        new_restaurant = Restaurant(name=name, address=address, added_by=blockstackuser)
        new_restaurant.save()
        return redirect('restaurant', id=new_restaurant.id)

class ReviewView(View):

#create a review
    def post(self, request, *args, **kwargs):
        #hack so that HTML forms can DELETE and PUT
        if request.POST.get('method') == 'delete':
            return self.delete(request, *args, **kwargs)
        
        username = request.COOKIES['username']
        email = request.COOKIES['email']

        restaurantid = request.POST['restaurantid']
        stars = request.POST['new_stars']
        description = request.POST['description']
        if int(stars) > 5 or int(stars) < 1:
            raise SuspiciousOperation('Invalid Stars')
        blockstackuser = blockstackUserForName(username)
        if not blockstackuser:
            #create a new user
            blockstackuser = BlockstackUser(email=email, username=username)
            blockstackuser.save()
    
        review = Review(user=blockstackuser, restaurant_id=restaurantid, stars=stars, description=description)           
        review.save()
        return redirect('restaurant', id=restaurantid)
        #return JsonResponse({'id':review.id})
    
    def delete(self, request, *args, **kwargs):
        review_id = request.POST['review_id']
        #check that review is owned by blockstackuser
        username = request.COOKIES['username']
        user = blockstackUserForName(username)
        if not user:
           return HttpResponseForbidden()
        rest = Review.objects.get(pk=review_id).restaurant
        Review.objects.filter(user=user,id=review_id).delete()
        return redirect('restaurant', id=rest.id)
    
    #for editing reviews
    def put(self, request, *args, **kwargs):
        review_id = request.POST['review_id']
        #check that review is owned by blockstackuser
        username = request.COOKIES['username']
        user = blockstackUserForName(username)
        if not user:
           return HttpResponseForbidden()
        review = Review.objects.get(user=user,id=review_id)
        review.stars = request.POST['stars']
        review.description = request.POST['description']
        review.save()  
        return JsonResponse('{"status":"ok"}')

def model_to_dict_extra(instance, fields=None, exclude=None, extra_fields=None):
    d = model_to_dict(instance, fields, exclude)
    if extra_fields:
        for f in extra_fields:
            d[f] = getattr(instance, f)
    return d
