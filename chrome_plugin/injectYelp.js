function aroundTheBlock() {
  function extractRestaurantAddress(){
      var street = document.querySelector('address span[itemprop="streetAddress"]').textContent;
      var locality = document.querySelector('address span[itemprop="addressLocality"]').textContent;
      var region = document.querySelector('address span[itemprop="addressRegion"]').textContent;
      var postalCode = document.querySelector('address span[itemprop="postalCode"]').textContent;
    
      return `${street}, ${locality}, ${region} ${postalCode}`;
  }

  function extractRestaurantName()
  {
	return document.querySelector('meta[itemprop="name"]:not([id="sitename"])').getAttribute('content');
  }

  //send the restaurant name and address to the app server
  req = new XMLHttpRequest();
  //req.open('POST', 'https://127.0.0.1/api/search')

  var fd = new FormData();
  fd.append('name', extractRestaurantName());
  fd.append('address', extractRestaurantAddress());

    const url = 'https://aroundtheblock.jumpstartups.io/api/search';
    //const url = 'http://127.0.0.1:8000/api/search'; //for testing
    
  fetch(url, {method:'POST', body: fd})
	.then( (response) => response.json())
	.then( (json) => renderReviews(json));


  function renderReviews(info)
    {
	console.log(info);
	var insertionElement = document.querySelectorAll('section')[6];
	var reviews = document.createElement('div');
	reviews.classList.add('aroundtheblock-reviews');
	var poweredby = document.createElement('div');
	poweredby.innerHTML = 'Reviews from blockstack users via <a href="https://aroundtheblock.jumpstartsups.io">Around the Block</a>';
	reviews.appendChild(poweredby);
	
	if(info.reviews)
	{
	    for(var review of info.reviews)
	    {
		var reviewDiv = document.createElement('div');
		reviewDiv.classList.add('aroundtheblock-review');

		const img = document.createElement('img');
		img.setAttribute('src', 'https://blog.blockstack.org/wp-content/uploads/2018/11/Blockstack-Icon-2.png');
		img.classList.add('aroundtheblock-img');
		reviewDiv.appendChild(img);
		
		var username = document.createElement('div');
		username.classList.add('aroundtheblock-user');
		username.textContent = review.username + ' says:';
		reviewDiv.appendChild(username);

		
		var stars = document.createElement('div');
		stars.classList.add('aroundtheblock-stars');
		for(var i=0; i < parseInt(review.stars); i++)
		{
		    stars.textContent += '★';
		}
		for(var i=0; i < 5 - parseInt(review.stars); i++)
		{
		    stars.textContent += '☆';
		}
		reviewDiv.appendChild(stars);

		
		var description = document.createElement('span');
		description.textContent = review.description;
		reviewDiv.appendChild(description);
		
		reviews.appendChild(reviewDiv);
	    }
	}
	else //render empty review list
	{
	    const sorry = document.createElement('div');
	    sorry.textContent = 'Sorry, blockstack users have not yet reviewed this restaurant.';
	    reviews.appendChild(sorry)
	}
	    insertionElement.insertBefore(reviews, insertionElement.querySelector('div:nth-child(2)'));
	
    }
};

aroundTheBlock();

